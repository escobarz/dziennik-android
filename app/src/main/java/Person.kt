import java.io.Serializable

class Person constructor(
    val pesel: String,
    var firstName: String,
    var lastName: String,
) : Serializable