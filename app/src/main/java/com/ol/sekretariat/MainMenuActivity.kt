package com.ol.sekretariat

import Person
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.ol.sekretariat.databinding.ActivityMainMenuBinding
import java.io.Serializable

class MainMenuActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainMenuBinding
    private var people: List<Person> = listOf(Person("123", "Jan", "Kowalski"))
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainMenuBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }

    fun onButtonClicked(view: View) {
        if (view !is Button) return
        when (view) {
            binding.buttonListOfPeople -> {
                val intent = Intent(this, PeopleListActivity::class.java).apply {
                    putExtra("people", people as Serializable)
                }
                startActivity(intent)
            }
        }
    }

    override fun onBackPressed() {
        //super.onBackPressed()
    }
}