package com.ol.sekretariat

import Person
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.ol.sekretariat.databinding.ActivityPersonBinding

class PersonActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPersonBinding
    private lateinit var currentPerson: Person
    private lateinit var firstName: String
    private lateinit var lastName: String
    private val fullName: String
        get() {
            return "$firstName $lastName"
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPersonBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        currentPerson = intent.getSerializableExtra("person") as Person
        firstName = currentPerson.firstName
        lastName = currentPerson.lastName
        binding.editTextFirstName.setText(firstName)
        binding.editTextLastName.setText(lastName)
        binding.textViewFullName.text = fullName
    }

    fun onButtonSaveClicked(view: View) {

    }
}