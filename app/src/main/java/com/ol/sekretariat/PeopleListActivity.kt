package com.ol.sekretariat

import Person
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ol.sekretariat.databinding.ActivityPeopleListBinding

class PeopleListActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPeopleListBinding
    private lateinit var people: List<Person>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPeopleListBinding.inflate(layoutInflater)
        setContentView(binding.root)
        people = intent.getSerializableExtra("people") as List<Person>
    }
}